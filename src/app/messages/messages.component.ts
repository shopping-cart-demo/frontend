import {Component, OnInit} from '@angular/core';
import {RxStompService} from '@stomp/ng2-stompjs';
import {HttpClient} from '@angular/common/http';
import {DiffPatcher} from 'jsondiffpatch';


declare const jsondiffpatch: any;
declare const jsonpatch: any;

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  public receivedMessages: Message[] = [];
  public diffs: any[] = [];
  private userId = this.rxStompService.stompClient.connectHeaders.login;
  public fsmDetails: string[] = [];
  public diffPatcher: DiffPatcher;
  public cart: any = {products: []};


  constructor(private rxStompService: RxStompService, private http: HttpClient) {

    this.diffPatcher = new DiffPatcher({
      // used to match objects when diffing arrays, by default only === operator is used
      objectHash: (obj) => {
        // this function is used only to when objects are not equal by ref
        return obj._id || obj.id;
      },
      arrays: {
        // default true, detect items moved inside the array (otherwise they will be registered as remove+add)
        detectMove: false,
        // default false, the value of items moved is not included in deltas
        includeValueOnMove: false
      },
      textDiff: {
        // default 60, minimum string length (left and right sides) to use text diff algorythm: google-diff-match-patch
        minLength: 60
      },
      propertyFilter: (name, context) => {
        /*
         this optional function can be specified to ignore object properties (eg. volatile data)
          name: property name, present in either context.left or context.right objects
          context: the diff context (has context.left and context.right objects)
        */
        return name.slice(0, 1) !== '$';
      },
      cloneDiffValues: false
    });

  }

  ngOnInit(): void {
    this.rxStompService.watch('/topic/greetings').subscribe((message: any) => {
      this.receivedMessages.push({message: message.body, type: 'INFO'});
    });

    this.rxStompService.watch('/topic/errors').subscribe((message: any) => {
      this.receivedMessages.push({message: message.body, type: 'ERROR'});
    });

    this.rxStompService.watch('/topic/diffs').subscribe((message: any) => {
      const diffSet = JSON.parse(message.body) as any[];
      console.log('Received diffs', message.body, diffSet);
      this.cart = jsonpatch.applyPatch(this.cart, diffSet).newDocument;

      diffSet.forEach((item) => {
        console.log('pushing item', item);
        this.diffs.push(item);
      });
    });
  }

  orderProduct(message: any) {
    this.rxStompService.publish({
      destination: '/app/hello',
      headers: {userId: this.userId},
      body: JSON.stringify(message)
    });
  }

  details(message: Message) {
    if (message.type === 'ERROR') {
      return;
    }
    this.http.get('/fsm/' + message.message).subscribe(
      (result: string[]) => {
        this.fsmDetails = result;

        setTimeout(() => {
          this.fsmDetails.forEach((detail, i) => {
            if (i === this.fsmDetails.length) {
              return;
            }
            const left = JSON.parse(this.fsmDetails[i]);
            const right = JSON.parse(this.fsmDetails[i + 1]);
            const delta = this.diffPatcher.diff(left, right);

            document.getElementById('placeholder' + i).innerHTML = jsondiffpatch.formatters.html.format(delta, left);
          });
        }, 200);
      }
    );
  }

}

interface Message {
  message: string;
  type: 'INFO' | 'ERROR';
}
